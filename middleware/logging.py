from django.utils.deprecation import MiddlewareMixin
from loguru import logger as LOG


class RequestLoggerMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request._body_to_log = request.body

    def process_response(self, request, response):
        if not hasattr(request, "_body_to_log"):
            return response

        msg = f"Request:'{request.user}' ID:{getattr(request.user, 'id', 0)} ({request.META.get('HTTP_X_REAL_IP')}) path:'{request.path}'"
        print(msg)
        extra = {
            "request_method": request.method,
            "request_path": request.path,
            "request_body": request._body_to_log,
            "request_ip": request.META.get("HTTP_X_REAL_IP"),
            "response_status_code": response.status_code,
            "request_user_id": request.user.id,
        }

        # Do not include settings in logging.
        extra.update({x: request.META[x] for x in request.META if x.isupper()})

        # Exclude passwords in logins.
        if b"password=" in request._body_to_log:
            extra["request_body"] = None

        LOG.info(msg, extra=extra)
        return response
