FROM python:3.7-slim as base

RUN apt-get update \
    && apt-get install -y \
    autoconf \
    automake \
    build-essential \
    default-libmysqlclient-dev \
    gcc \
    git \
    libssl-dev \
    libtool \
    make \
    pkg-config \
    gcc \
    python-dev libxml2-dev libxmlsec1-dev \
    wget \
    && rm -rf /var/lib/apt/lists/*

# ----------------------------
# 1. Python dependencies build stage
# ----------------------------
FROM base as builder

RUN apt-get update && apt-get install -y \
    wget \
    git \
    gcc \
    default-libmysqlclient-dev \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y libpq-dev

# Install python requirements.
WORKDIR /wheels
COPY ./requirements.txt /wheels/requirements.txt
RUN pip3 wheel -r requirements.txt

# ----------------------------
# 2. Merge stage.
# ----------------------------
FROM base

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get install -y \
    git \
    nginx \
    default-libmysqlclient-dev \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /wheels /wheels
RUN pip3 install -r /wheels/requirements.txt -f /wheels \
        && rm -rf /wheels \
        && rm -rf /root/.cache

COPY infrastructure/ken-nginx.conf /etc/nginx/sites-available/ken-proxy
RUN ln -s /etc/nginx/sites-available/ken-proxy /etc/nginx/sites-enabled/ken-proxy && \
                 rm /etc/nginx/sites-enabled/default

COPY . /var/www/html

WORKDIR /var/www/html

RUN ls /var/www/html
