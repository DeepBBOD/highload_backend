#!/usr/bin/env python3
import os
import sys
from django.core.management import execute_from_command_line

from innocook.logging import setup_logging


if __name__ == "__main__":
    if "test" in sys.argv:
        print("Using settings for test.")
        os.environ["DJANGO_SETTINGS_MODULE"] = "innocook.settings.local"
    else:
        print("Using production settings.")
        os.environ["DJANGO_SETTINGS_MODULE"] = "innocook.settings.local"

    setup_logging()

    execute_from_command_line(sys.argv)
