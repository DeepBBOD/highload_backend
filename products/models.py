import json

from django.db import models


# Create your models here.
class Category(models.Model):
    name = models.CharField(blank=False, null=False, max_length=125, unique=True)
    image_url = models.CharField(blank=True, null=True, max_length=1024, unique=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return "(%s)" % (self.name,)

    def __unicode__(self):
        return "(%s)" % (self.name,)

    def to_json(self):
        selfdict = dict(name=self.name,)

        return json.dumps(selfdict)


# Create your models here.
class SubCategory(models.Model):
    name = models.CharField(blank=False, null=False, max_length=125, unique=True)
    image_url = models.CharField(blank=True, null=True, max_length=1024, unique=True)
    image = models.ImageField(null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return "(%s)" % (self.name,)

    def __unicode__(self):
        return "(%s)" % (self.name,)

    def to_json(self):
        selfdict = dict(name=self.name,)

        return json.dumps(selfdict)


# Create your models here.
class Product(models.Model):
    name = models.CharField(blank=False, null=False, max_length=125, unique=True)
    description = models.CharField(blank=True, null=True, max_length=1024, unique=True)
    image_url = models.CharField(blank=True, null=True, max_length=1024, unique=True)
    image = models.ImageField(null=True, blank=True)
    price = models.FloatField(default=0, blank=False, null=False)
    rating = models.FloatField(default=-1)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE)

    def __str__(self):
        return "(%s)" % (self.name,)

    def __unicode__(self):
        return "(%s)" % (self.name,)

    def to_json(self):
        selfdict = dict(name=self.name,)

        return json.dumps(selfdict)
