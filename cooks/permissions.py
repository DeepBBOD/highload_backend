from rest_framework import permissions


class PostOnlyCookPermission(permissions.BasePermission):
    group_name = "cooks"

    def has_permission(self, request, view):
        """
        Should simply return True, or raise a 403 response.
        """
        if request.method != "GET":
            return request.user.is_cook
        return True
