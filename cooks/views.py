from __future__ import absolute_import

from datetime import datetime

import ujson as json
from django.http.response import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import View
from loguru import logger as LOG
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.views import Response

from cooks.models import CustomUser
from cooks.permissions import PostOnlyCookPermission
from cooks.serializers import CookSerializer
from cooks.serializers import MenuItemDetailSerializer
from cooks.serializers import PlaceMenuItemSerializer
from es.hashes import get_item_hash
from es.utils import get_elasticsearch_client
from es.utils import search_fuzzy
from es.utils import update_elasticsearch_items


class UserData(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        serializer = CookSerializer(self.request.user)
        return JsonResponse(serializer.data)


class ProjectAutoCompleteQueryView(View):
    """
    Main search view. Supports pagination per resource and complicated
    aggregations.
    """

    def return_error(self, error_message="Missing query parameter"):
        return HttpResponse(
            status=400,
            content='{"error": "%s"}' % error_message,
            content_type="application/json",
        )

    def get(self, request, *args, **kwargs):
        try:
            data = json.loads(request.GET["parameters"])

            # Unpack data
            start_from = data.get("from", 0)
            query = data.get("q", "*")
            filters = data.get("filters", {})
            aggregations = data.get("aggregations", "products_by_categories")
            aggregations = aggregations.replace(":", ";")
            range_filters = data.get("range", {})
            sorting_method = data.get("sorting_method", {})

            size = data.get("size", 10)
            size = 50 if size > 50 else size

            if not query:
                return self.return_error()

            # TODO remove service, product, scope
            # TODO reduce function invocation arguments.
            ret_list = search_fuzzy(
                query=query,
                start_from=start_from,
                size=size,
                filters=filters,
                range_filters=range_filters,
                sorting_method=sorting_method,
                aggregations=aggregations,
            )

            response = ret_list["hits"]
            response["aggregations"] = ret_list.pop("aggregations", None)
            response["query"] = ret_list.pop("query", None)
            response["profile"] = ret_list.pop("profile", None)
            response["took"] = ret_list.pop("took", None)
            return HttpResponse(json.dumps(response), content_type="application/json")

        except Exception as e:
            LOG.exception(f"Elasticsearch view error: {repr(e)}")
            return self.return_error(f"Search error: {e}")


class MenuView(APIView):
    """
    Provides a get method handler.
    """

    serializer_class = PlaceMenuItemSerializer
    permission_classes = [IsAuthenticated, PostOnlyCookPermission]

    # permission_classes = [IsAuthenticated]

    def post(self, request, cook_id, *args, **kwargs):
        if not request.user.is_cook:
            raise PermissionDenied("Only cook can create menu items")

        if cook_id != request.user.id:
            raise PermissionDenied("Only owner can create menu items")

        serializer = self.serializer_class(data=request.POST)
        if serializer.is_valid(raise_exception=True):
            title = serializer.validated_data["title"]
            description = serializer.validated_data["description"]
            category = serializer.validated_data["category"]
            sub_category = serializer.validated_data["sub_category"]
            cost = serializer.validated_data["cost"]
            region = serializer.validated_data["region"]
            user_id = cook_id

            item_hash = get_item_hash(
                resource_name=title,
                category=category,
                sub_category=sub_category,
                region=region,
                user_id=user_id,
            )
            ingestion_document = {
                "_id": item_hash,
                "_source": {
                    "title": title,
                    "description": description,
                    "category": category,
                    "sub_category": sub_category,
                    "cost": cost,
                    "is_cook_offer": True,
                    "done": False,
                    "is_deleted": False,
                    # Used for resource availability calculation
                    "user": {"id": user_id, "name": "test",},
                    "create_date": datetime.now(),
                    "region": region,
                },
            }
            client = get_elasticsearch_client()
            update_elasticsearch_items(
                client=client, item_list=[ingestion_document], refresh=True
            )

        print(json.dumps(request.POST.dict()))
        return HttpResponse()

    def get(self, request, cook_id, *args, **kwargs):
        import time

        filters = {
            "user.id": [cook_id],
        }
        if not request.GET.get("show_deleted"):
            filters["is_deleted"] = [False]
        if request.GET.get("status"):
            filters["status"] = [request.GET.get("status")]
        start = time.time()
        es_response = search_fuzzy(size=100, query="*", filters=filters)
        end = time.time()
        print(end - start)
        return JsonResponse({"menu": es_response.get("hits", {}).get("hits", [])})


class MenuItemDetailsView(APIView):
    serializer_class = MenuItemDetailSerializer
    permission_classes = [IsAuthenticated, PostOnlyCookPermission]

    def patch(self, request, cook_id, resource_id, *args, **kwargs):
        if cook_id != request.user.id:
            raise PermissionDenied("Only owner can change menu items")

        serializer = self.serializer_class(data=request.POST)
        if serializer.is_valid(raise_exception=True):
            ingestion_document = {
                "_id": resource_id,
                "_source": {},
            }
            for k, v in serializer.validated_data.items():
                if k in serializer.fields:
                    ingestion_document["_source"][k] = v
            client = get_elasticsearch_client()
            update_elasticsearch_items(
                client=client, item_list=[ingestion_document], refresh=True
            )
        return HttpResponse()

    def get(self, request, cook_id, resource_id, *args, **kwargs):
        filters = {
            "user.id": [cook_id],
            "_id": [resource_id],
        }
        es_response = search_fuzzy(size=100, query="*", filters=filters)
        return JsonResponse(es_response["hits"]["hits"][0])

    def delete(self, request, cook_id, resource_id, *args, **kwargs):
        if cook_id != request.user.id:
            raise PermissionDenied("Only owner can delete menu items")

        ingestion_document = {
            "_id": resource_id,
            "_source": {"is_deleted": True,},
        }
        client = get_elasticsearch_client()
        update_elasticsearch_items(
            client=client, item_list=[ingestion_document], refresh=True
        )
        return HttpResponse()


class CookView(generics.ListAPIView):
    queryset = CustomUser.objects.filter(is_cook=True)
    serializer_class = CookSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        if not request.GET.get("show_inactive"):
            queryset = queryset.filter(online=True)
        if request.GET.get("region"):
            queryset = queryset.filter(online=True, region=request.GET.get("region"))
        serializer = CookSerializer(queryset, many=True)
        return Response(serializer.data)


class CookDetailsView(APIView):
    queryset = CustomUser.objects.filter(is_cook=True)
    serializer_class = CookSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, cook_id, *args, **kwargs):
        queryset = self.queryset
        queryset = get_object_or_404(queryset, id=cook_id)
        serializer = CookSerializer(queryset)
        resp = serializer.data
        resp["menu"] = CustomUser.objects.get(id=cook_id).get_menu()
        return Response(resp)
