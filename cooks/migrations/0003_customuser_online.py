# Generated by Django 3.0.6 on 2020-05-09 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cooks", "0002_auto_20200509_2216"),
    ]

    operations = [
        migrations.AddField(
            model_name="customuser",
            name="online",
            field=models.BooleanField(default=True),
        ),
    ]
