from __future__ import absolute_import

from django.conf.urls import url
from django.urls import path
from rest_framework import routers

from cooks.views import CookDetailsView
from cooks.views import CookView
from cooks.views import MenuItemDetailsView
from cooks.views import MenuView
from cooks.views import ProjectAutoCompleteQueryView

urlpatterns = [
    url(
        r"^ac/query/$",
        ProjectAutoCompleteQueryView.as_view(),
        name="autocomplete_query",
    ),
    path("", CookView.as_view(), name="cook"),
    path("<int:cook_id>/", CookDetailsView.as_view(), name="cook_details"),
    path("<int:cook_id>/menu/", MenuView.as_view(), name="menu"),
    path(
        "<int:cook_id>/menu/<str:resource_id>/",
        MenuItemDetailsView.as_view(),
        name="menu_item",
    ),
]

router = routers.DefaultRouter()

urlpatterns += router.urls
