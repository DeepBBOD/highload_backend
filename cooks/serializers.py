from rest_framework import serializers

from cooks.models import CustomUser


class CookSerializer(serializers.ModelSerializer):
    menu = serializers.JSONField(required=False)

    class Meta:
        model = CustomUser
        fields = [
            "id",
            "email",
            "username",
            "date_joined",
            "is_client",
            "is_cook",
            "region",
            "online",
            "menu",
        ]


class PlaceMenuItemSerializer(serializers.Serializer):
    title = serializers.CharField(required=True, write_only=True)
    description = serializers.CharField(required=True, write_only=True)
    category = serializers.CharField(required=True, write_only=True)
    sub_category = serializers.CharField(required=True, write_only=True)
    cost = serializers.FloatField(required=True, write_only=True)
    region = serializers.CharField(required=True, write_only=True)


class MenuItemDetailSerializer(serializers.Serializer):
    title = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    category = serializers.CharField(required=False)
    sub_category = serializers.CharField(required=False)
    cost = serializers.FloatField(required=False)
    region = serializers.CharField(required=False)
    is_deleted = serializers.BooleanField(required=False)
