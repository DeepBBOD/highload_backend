from django.contrib.auth.base_user import BaseUserManager
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    def create_user(self, username, password1, password2, email=None, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not username:
            raise ValueError(_("The Username must be set"))

        if password1 != password2:
            raise ValidationError(_("Passwords don't match"))

        email = self.normalize_email(email)
        payload = {"username": username, "email": email, **extra_fields}

        if extra_fields.get("is_cook"):
            payload["is_cook"] = True
            payload["is_client"] = False

        user = self.model(**payload)
        user.set_password(password1)
        user.save()
        return user

    def create_superuser(
        self, username, password1, password2, email=None, **extra_fields
    ):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Superuser must have is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Superuser must have is_superuser=True."))
        return self.create_user(username, email, password1, password2, **extra_fields)
