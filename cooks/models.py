import json

from django.contrib.auth.models import AbstractUser
from django.db import models
from model_utils.models import TimeStampedModel

from es.utils import search_fuzzy
from .managers import CustomUserManager


# Create your models here.


class CustomUser(AbstractUser):
    is_client = models.BooleanField(default=True)
    is_cook = models.BooleanField(default=False)
    region = models.CharField(max_length=100, default="Innopolis")
    online = models.BooleanField(default=True)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def get_menu(self):
        if self.is_cook:
            filters = {
                "user.id": [self.id],
            }
            es_response = search_fuzzy(size=100, query="*", filters=filters)
            return es_response["hits"]["hits"]

    def __str__(self):
        return self.username


class ClientPaymentSource(TimeStampedModel):
    """Model class to represent a Client in the system.

    """

    payment_source = models.CharField(
        blank=True, null=True, max_length=125, unique=True
    )
    type = models.CharField(blank=False, max_length=255)

    def __str__(self):
        return "%s (%s-%s)" % (
            self.type,
            self.payment_source[:2],
            self.payment_source[-2:],
        )

    def __unicode__(self):
        return u"%s (%s-%s)" % (
            self.type,
            self.payment_source[:2],
            self.payment_source[-2:],
        )


class Client(TimeStampedModel):
    """
    Model class to represent a Client in the system.
    """

    admin = models.ManyToManyField(CustomUser, blank=True)
    name = models.CharField(blank=False, max_length=255)
    code = models.CharField(blank=False, unique=True, max_length=10)
    address = models.TextField(blank=True, max_length=255)
    payment_account_id = models.CharField(blank=True, max_length=255)
    payment_sources = models.ManyToManyField(ClientPaymentSource, blank=True)
    creation_date = models.DateTimeField(blank=True, null=True)
    logo = models.CharField(max_length=1024, null=True, blank=True)
    CLIENT_STATUS_ACTIVE = "active"
    CLIENT_STATUS_DELETED = "deleted"
    status = models.CharField(default=CLIENT_STATUS_ACTIVE, blank=False, max_length=255)

    def __str__(self):
        return "%s (%s)" % (self.name, self.code)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.code)

    def to_json(self):
        selfdict = dict(name=self.name, code=self.code)

        return json.dumps(selfdict)
