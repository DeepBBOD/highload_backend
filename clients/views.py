from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.http import Http404
from django.http.response import JsonResponse
from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from es.utils import place_order
from es.utils import search_fuzzy
from clients.serializers import UserSerializer
from cooks.models import CustomUser
from cooks.serializers import CookSerializer


class CreateUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]  # Or anon users can't register
    serializer_class = UserSerializer


class RegistrationView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        try:
            user = CustomUser.objects.create_user(
                **{k: v for k, v in request.POST.items()}
            )
            data = CookSerializer(user).data
            return JsonResponse({"user": data})
        except IntegrityError:
            return JsonResponse(
                {"error": True, "reason": "Username already exists"}, status=400
            )
        except ValidationError as e:
            return JsonResponse({"error": True, "reason": e.messages[0]}, status=400)


class PlaceOrderView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, item_id, *args, **kwargs):
        print(request.user)
        place_order(request, item_id)
        return JsonResponse({"OK": True})


class OrderView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        items = search_fuzzy(filters={"orders.user.id": [request.user.id],})["hits"][
            "hits"
        ]

        for item in items:
            orders = []
            for order in item["_source"].get("orders", []):
                if order["user.id"] == request.user.id:
                    if request.POST.get("hide_active") and order["done"]:
                        continue
                    if request.POST.get("hide_completed") and not order["done"]:
                        continue
                    orders.append(order)
            item["_source"]["orders"] = orders

        return JsonResponse({"orders": items})
