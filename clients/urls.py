from django.urls import path
from rest_framework import routers

from clients.views import OrderView
from clients.views import PlaceOrderView

urlpatterns = [
    path("", OrderView.as_view(), name="orders"),
    path("place_order/<str:item_id>/", PlaceOrderView.as_view(), name="place_order"),
]

router = routers.DefaultRouter()

urlpatterns += router.urls
