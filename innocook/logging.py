import logging
import os
import sys
from typing import Any
from typing import List

from django.conf import settings
from logstash_async.handler import AsynchronousLogstashHandler
from loguru import logger


class InterceptHandler(logging.Handler):
    def emit(self, record):
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelname, record.getMessage())


def setup_logging():
    handlers: List[Any] = [
        {"sink": sys.stderr, "level": "INFO", "backtrace": False, "diagnose": False}
    ]
    if settings.LOGSTASH_REPORT_URL:
        logstash_host, logstash_port = settings.LOGSTASH_REPORT_URL.rsplit(":", 1)
        handlers.append(
            {
                "sink": AsynchronousLogstashHandler(
                    host=logstash_host, port=int(logstash_port), database_path=None,
                ),
                "level": "INFO",
                "format": "{message}",
            }
        )

    logger.configure(
        handlers=handlers,
        extra={
            "debug": settings.DEBUG,
            "environment": "uat",
            "deploy_date": settings.DEPLOY_DATE,
            "worker_name": os.environ.get("CELERY_WORKER_NAME", "ken"),
        },
        activation=[
            ("", True),
            # Deactivate spam
            ("py.warnings", False),
            ("urllib3", False),
            ("ddtrace.internal.writer", False),
        ],
    )


def setup_debug_logging():
    """Function used manually to investigate possible issues."""
    handlers: List[Any] = [
        {"sink": sys.stderr, "level": "DEBUG", "backtrace": False, "diagnose": False}
    ]

    # if settings.DEBUG:
    #     handlers.append({"sink": "/tmp/debug.log", "level": "DEBUG"})

    logger.configure(
        handlers=handlers,
        extra={
            "debug": settings.DEBUG,
            "environment": settings.KEN_ENV,
            "deploy_date": settings.DEPLOY_DATE,
            "worker_name": os.environ.get("CELERY_WORKER_NAME", "ken"),
        },
        activation=[
            ("", True),
            # Deactivate spam
            ("botocore", False),
            ("boto.connection", False),
            ("py.warnings", False),
            ("urllib3", False),
            ("ddtrace.internal.writer", False),
            ("elasticsearch", False),
            ("elasticsearch.connection.base", False),
        ],
    )
