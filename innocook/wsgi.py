"""
WSGI config for innocook project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from whitenoise import WhiteNoise
from innocook.logging import InterceptHandler
from innocook.logging import setup_logging
import logging

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "innocook.settings.local")

application = get_wsgi_application()
application = WhiteNoise(application, root="static")

# Intercept logs
logging.basicConfig(handlers=[InterceptHandler()], level=0)
setup_logging()
