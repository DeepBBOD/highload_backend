#!/usr/bin/env python3

"""
Usage: ./ken.py
"""
import os
import shlex
import signal
import subprocess
import sys
import time

from infrastructure.utils import GracefulKiller

# Setup environment
os.environ.setdefault("CELERY_CONFIG_MODULE", "ken.settings.celeryconfig")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ken.settings.local")

gzip_nginx = """echo "gzip_proxied any; \ngzip_types text/plain text/xml text/css application/x-javascript application/javascript; \ngzip_vary on;" > /etc/nginx/conf.d/gzip.conf"""
os.system(gzip_nginx)


NAME = "ken"
DJANGODIR = "/var/www/html"
USER = "www-data"
GROUP = "www-data"
NUM_WORKERS = 5
NUM_THREADS = 10
MAX_REQUESTS = 2000
DJANGO_WSGI_MODULE = "ken.wsgi"  # WSGI module name
DJANGO_SETTINGS_MODULE = "innocook.settings.production"

IP = "0.0.0.0"
TIMEOUT = 30


def main():
    # Format keysets with environment variables and fill prod settings.
    print("Warmup done.")

    # Stop/start nginx and define launching string.
    # if os.environ["ken_debug"] == "False":
    #     os.system("service nginx start")
    #     LAUNCH_COMMAND = f"""
    # uwsgi --http {IP}:8000
    #       --module {DJANGO_WSGI_MODULE}
    #       --procname-prefix {NAME}
    #       --gevent {NUM_THREADS}
    #       --workers {NUM_WORKERS}
    #       --max-requests {MAX_REQUESTS}
    #       --uid {USER}
    #       --gid {GROUP}
    #       --harakiri {TIMEOUT}
    #       --buffer-size 8192
    #       --master
    #       --die-on-term
    #       --limit-post 51200000
    # """
    # else:
    LAUNCH_COMMAND = f"./manage.py runserver_plus {IP}:80"

    print(LAUNCH_COMMAND)
    return LAUNCH_COMMAND


if __name__ == "__main__":
    LAUNCH_COMMAND = main()
    print("-----------------------")
    print("Launching Ken")
    print("-----------------------")
    killer = GracefulKiller()

    if "migrate" in sys.argv:
        print("Running migration")

        exit_code = os.system(
            f'yes "yes" | ./manage.py migrate cloudtrail --database=cloudtrail_db --noinput --settings={DJANGO_SETTINGS_MODULE}'
        )

        exit_code = os.system(
            f'yes "yes" | ./manage.py migrate --noinput --settings={DJANGO_SETTINGS_MODULE}'
        )
        print("Exit code: ", exit_code)
        sys.exit(exit_code)

    p = subprocess.Popen(shlex.split(LAUNCH_COMMAND))

    while True:
        sys.stdout.flush()
        time.sleep(1)
        if killer.kill_now:
            print("Sending termination signal to Ken")
            p.send_signal(signal.SIGTERM)
            killer.kill_now = False

        if p.poll() != None:
            print("Ken finished gracefully.")
            break
