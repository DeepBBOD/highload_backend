import redis
import six.moves.urllib.parse
from contextlib import contextmanager
from loguru import logger as LOG

from django.conf import settings

REDIS_URI = six.moves.urllib.parse.urlparse(settings.BROKER_URL)
REDIS_HOST = REDIS_URI.hostname
REDIS_PORT = REDIS_URI.port
REDIS_CONNECTION = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)  # type: ignore


@contextmanager
def task_lock(key, timeout=None, prefix="lock", release_lock=True, blocking=False):
    """
    Task decorator to work with only one task.
    Keys are stored in Redis being prefixed with `prefix`
    """

    lock_name = f"{prefix}:{key}"
    lock = REDIS_CONNECTION.lock(lock_name, timeout=timeout)

    have_lock = lock.acquire(blocking=blocking)

    try:
        if have_lock:
            LOG.debug(f"Locking task {lock_name} for {timeout} seconds")
        else:
            LOG.warning(f"Task {lock_name} is locked!")
        yield have_lock

    finally:

        try:
            if have_lock and release_lock:
                lock.release()

        except Exception as e:
            LOG.debug(f"Cannot release unlocked lock: {e}")
