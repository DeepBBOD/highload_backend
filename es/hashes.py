import hashlib

from loguru import logger as LOG

from es.exceptions import WrongHashException


# TODO add checks for ARN presence for applicable resources.
def get_item_hash(
    resource_name: str, category: str, sub_category: str, region: str, user_id=""
) -> str:
    """
    Args:
        resource_id: `item_id` field in mapping.
        product: `product` field in mapping.
        account_number: `project.account_number` field in mapping.
        region: `region` field in mapping.
    """

    if isinstance(user_id, int):
        user_id = str(user_id)

    if not category:
        raise WrongHashException("Missing category for getting hash")

    if not sub_category:
        raise WrongHashException("Missing subcategory for getting hash")

    if not resource_name:
        raise WrongHashException("Missing resource_name for getting hash")

    if not region:
        LOG.debug("Region is not passed, translating to `global`")
        region = "global"

    return hashlib.md5(
        bytes(f"{resource_name}-{category}-{sub_category}-{region}-{user_id}", "utf-8")
    ).hexdigest()
