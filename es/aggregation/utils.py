from loguru import logger as LOG
from datetime import timedelta, datetime
from typing import Dict
from typing import List
from typing import Any

from elasticsearch_dsl import A
from elasticsearch_dsl import Q
from elasticsearch_dsl import Search
from elasticsearch_dsl.aggs import Filter


class ResourceAggregation(object):
    def __init__(
        self,
        search_dsl: Search,
        start_from: int = 0,
        size: int = 100,
        sorting_method: Dict[str, Dict[str, str]] = None,
        agg_nested_filters: List[Any] = None,
        is_war_page: bool = False,
    ):
        self.search_dsl = search_dsl
        self.start_from = start_from
        self.size = size
        self.sorting_method = sorting_method

        self.agg_nested_filters = agg_nested_filters or []
        self.is_war_page = is_war_page

    def __getitem__(self, key):
        if key.startswith("__") or key not in dir(self):
            raise ValueError(f"Wrong agg name: {key}")

        getattr(self, key)()
        return self.search_dsl

    def __extend_range(self, start=None, end=None):
        """
        Function extends range of query filter from outermost scope.
        """
        LOG.info(f"Extending query range automatically to {start} - {end}")
        if start:
            self.query_cost_from = start
        if end:
            self.query_cost_to = end

    def __remove_outer_range_filter(self):
        LOG.info(f"Removing outer filter for cost_to and cost_from")
        self.query_cost_from = None
        self.query_cost_to = None

    def resources_hits(self):
        self.search_dsl.aggs.bucket("resources_hits", "top_hits", size=100)

    def products_by_categories(self):
        categories_agg = A("terms", field="category", size=50)
        subcategories_agg = categories_agg.bucket(
            "sub_categories", "terms", field="sub_category", size=50
        )

        subcategories_agg.bucket(
            "results", "top_hits", _source={"exclude": ["client",]},
        )

        self.search_dsl.aggs.bucket("products_by_categories", categories_agg)

    def categories_by_regions(self):
        regions_agg = A("terms", field="region", size=50)
        categories_agg = regions_agg.bucket(
            "categories", "terms", field="category", size=50
        )
        categories_agg.bucket(
            "results", "top_hits", _source={"exclude": ["client",]},
        )

        self.search_dsl.aggs.bucket("categories_by_regions", regions_agg)
        self.__remove_outer_range_filter()
