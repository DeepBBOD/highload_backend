class BaseElasticSearchException(Exception):
    pass


class EmptyIndexListException(BaseElasticSearchException):
    """Raised on empty list passed to elasticsearch client"""


class MissingSearchScope(BaseElasticSearchException):
    """Raised when no index name is passed"""


class WrongItemFormat(BaseElasticSearchException):
    """Raised during ingestion when wrong item format is passed"""


class WrongHashException(BaseElasticSearchException):
    """Raised on every hashing issue"""
