import copy
from collections import defaultdict
from datetime import datetime
from typing import Any
from typing import Dict
from typing import List

import elasticsearch.client
from django.conf import settings
from django.http import Http404
from elasticsearch import helpers
from elasticsearch.client import Elasticsearch
from elasticsearch.exceptions import RequestError
from elasticsearch.helpers import BulkIndexError
from elasticsearch_dsl import Search
from loguru import logger as LOG

from celery.utils import task_lock
from es.aggregation.utils import ResourceAggregation
from es.constants import ELASTICSEACH_SEARCH_FIELDS
from es.constants import ES_ESCAPE_RULES
from es.exceptions import EmptyIndexListException
from es.exceptions import WrongItemFormat


class ElasticsearchSafe(Elasticsearch):
    """ This class is intended to be wrapped with `elasticsearch_safety_decorator` """

    def search(self, *args, **kwargs):
        index = kwargs.get("index")
        if not index or index == "*":
            raise EmptyIndexListException
        return super().search(*args, **kwargs)


def get_elasticsearch_client(
    timeout: int = settings.ELASTICSEARCH_INGESTION_TIMEOUT, unsafe: bool = False
):
    connection_kwargs = {
        "hosts": settings.ELASTIC_SEARCH_NODES,
        "timeout": timeout,
        # "http_compress": True,
    }
    if unsafe:
        LOG.warning("Getting unsafe elasticsearch client.")
        client = Elasticsearch(
            **connection_kwargs,
            http_auth=(settings.ELASTICSEARCH_USER, settings.ELASTICSEARCH_PASSWORD),
        )
    else:
        client = ElasticsearchSafe(
            **connection_kwargs, http_auth=("elastic", "changeme")
        )

    return client


def escapedSeq(term):
    """ Yield the next string based on the
        next character (either this char
        or escaped version """
    for char in term:
        if char in ES_ESCAPE_RULES:
            yield ES_ESCAPE_RULES[char]
        else:
            yield char


def escapeQS(term):
    """ Apply escaping to the passed in query terms escaping special characters like - , etc"""
    # Do not escape empty searches.
    if term == "*":
        return term

    term = term.replace("\\", r"\\")  # escape \ first
    return "".join(escapedSeq(term))


# Single condition
def _transform_filters_dict(
    filters_data, condition, formatted_filters, agg_nested_filters
):
    for element in filters_data:
        # Handle nested filters:
        # {"nested:cost.operation:cost.operation.item_type": ["RunInstances"]}
        if "nested:" in element:
            _, path, field = element.split(":")

            filter_values = filters_data[element]
            agg_type = "terms" if isinstance(filter_values, list) else "term"
            terms_query = {agg_type: {field: filter_values}}

            nested_filter = {"path": path, "query": terms_query}
            formatted_filters[condition].append({"nested": nested_filter})

            # Add filters to aggreagtion filters
            agg_nested_filters.append(
                {"path": path, "query": {"bool": {condition: terms_query}}}
            )

        else:
            filter_values = filters_data[element]
            agg_type = "terms" if isinstance(filter_values, list) else "term"
            query = {agg_type: {element: filters_data[element]}}
            formatted_filters[condition].append(query)

    return formatted_filters, agg_nested_filters


# Multiple conditions
def _transform_filters_list(
    filters_data, condition, formatted_filters, agg_nested_filters
):
    filter_keys = [element.keys() for element in filters_data]

    for filter_node in filters_data:

        nested_bool_query = defaultdict(list)

        # Handle logic on duplicated filters. Passing in duplicated filters means searching for one of.
        if filter_keys.count(filter_node.keys()) > 1 and condition == "must":
            duplicated_keys = True
        else:
            duplicated_keys = False

        for element in filter_node:
            if "nested:" in element:
                _, path, field = element.split(":")
                filter_values = filter_node[element]
                agg_type = "terms" if isinstance(filter_values, list) else "term"
                terms_query = {agg_type: {field: filter_values}}

                nested_filter = {"path": path, "query": terms_query}

                nested_bool_query[condition].append({"nested": nested_filter})

                # Add filters to aggreagtion filters
                agg_nested_filters.append(
                    {"path": path, "query": {"bool": {condition: terms_query}}}
                )

            else:
                filter_values = filter_node[element]
                agg_type = "terms" if isinstance(filter_values, list) else "term"
                query = {agg_type: {element: filter_node[element]}}
                nested_bool_query[condition].append(query)

        if duplicated_keys:
            formatted_filters["should"].append({"bool": dict(nested_bool_query)})
            formatted_filters["minimum_should_match"] = 1

        else:
            formatted_filters["must"].append({"bool": dict(nested_bool_query)})

    return formatted_filters, agg_nested_filters


def transform_filters(
    filters_data, condition, formatted_filters=None, agg_nested_filters=None,
):
    """
    filters_data:
    condition: Either: must, must_not, should
    formatted_filters: Filters that are applied to query scope
    nested_filters: Used in aggregations to filter results further. Must be removed eventually in favor of using formatted_filters.
    """

    formatted_filters = formatted_filters or defaultdict(list)
    agg_nested_filters = agg_nested_filters or []

    # Single condition
    if isinstance(filters_data, dict):
        formatted_filters, agg_nested_filters = _transform_filters_dict(
            filters_data=filters_data,
            condition=condition,
            formatted_filters=formatted_filters,
            agg_nested_filters=agg_nested_filters,
        )

    if isinstance(filters_data, list):
        formatted_filters, agg_nested_filters = _transform_filters_list(
            filters_data=filters_data,
            condition=condition,
            formatted_filters=formatted_filters,
            agg_nested_filters=agg_nested_filters,
        )

    return formatted_filters, agg_nested_filters


def search_fuzzy(
    query: str = "*",
    start_from: int = 0,
    size: int = 50,
    filters: Dict[str, List[str]] = None,
    range_filters: Dict[str, List[str]] = None,
    sorting_method: Dict[str, Dict[str, str]] = None,
    index_name: str = settings.DEFAULT_INDEX,
    aggregations: str = "products_by_categories",
    show_active: Any = None,
):
    """
    Function to power whole nOps. Does search in Elasticsearch, aggregates results using `aggregations`.

    Args:
        query: Query to perform search. Omit this parameter for all results.
        start_from: Where pagination starts from.
        size: Page size for pagination.
        filters: Filters to filter results against. Format: `{"region": ["us-west-2"]}`
        range_filters: Filters applied to ranges (dates, values)
        sorting_method: Sorting.
        index_name: Required. Index/es to perform search on.
        aggregations: Aggregations to apply. Split multiple aggs with `;`. List of aggregations can be found in `libs/search/aggregation.py`
        show_active: Deprecated. Filters should be used instead. True for active, False for inactive, and None for all entries.

    Returns:
        dict:
    """
    # Parse query.
    query = query.lower()
    words = query.split()
    query = " AND ".join(["*{}*".format(escapeQS(word)) for word in words])

    match_all = False
    if query == "***":
        match_all = True

    if not index_name:
        raise ValueError("No index passed")

    search_dsl = Search()

    # Define query.
    if match_all:
        search_dsl = search_dsl.query("match_all")
    else:
        search_dsl = search_dsl.query(
            "query_string", query=query, fields=ELASTICSEACH_SEARCH_FIELDS
        )

    if show_active is not None:
        search_dsl = search_dsl.query("match", active=show_active)

    # Filters part
    # Nested filter example:
    # {"nested:cost.operation:cost.operation.item_type": ["RunInstances"]}

    # Used on query scope.
    formatted_filters: List[Any] = []  # noqa
    # Used inside aggregations.
    agg_nested_filters: List[Any] = []  # noqa
    if filters:
        # Prevent change of filters
        filters_copy = copy.deepcopy(filters)

        is_multiple_conditions = filters_copy.pop("_multiple_conditions", False)
        if is_multiple_conditions:
            for condition in filters_copy:
                formatted_filters, agg_nested_filters = transform_filters(
                    filters_data=filters_copy[condition],
                    condition=condition,
                    formatted_filters=formatted_filters,
                    agg_nested_filters=agg_nested_filters,
                )

        # old must condition style.
        else:
            formatted_filters, agg_nested_filters = transform_filters(
                filters_data=filters_copy,
                condition="must",
                agg_nested_filters=agg_nested_filters,
            )

        if formatted_filters:
            search_dsl = search_dsl.query("bool", **dict(formatted_filters))

    # TODO support multiple conditions
    # TODO rework
    if range_filters:
        is_nested = False
        for element in range_filters:
            if "nested:" in element:
                is_nested = True
                _, path, field = element.split(":")
                nested_query = {
                    "bool": {"must": [{"range": {field: range_filters[element]}}]}
                }
                search_dsl = search_dsl.query("nested", path=path, query=nested_query)

                agg_nested_filters.append({"path": path, "query": nested_query})

        if not is_nested:
            search_dsl = search_dsl.query(
                "constant_score", filter={"range": range_filters}
            )

    # Define sorting:
    if not sorting_method:
        sorting_method = {
            "current_month_cost": {"order": "desc", "unmapped_type": "float"}
        }

    agg_factory = ResourceAggregation(
        search_dsl=search_dsl,
        start_from=start_from,
        size=size,
        sorting_method=sorting_method,
        agg_nested_filters=agg_nested_filters,
    )

    # for agg_name in aggregations.split(";"):
    #     search_dsl = agg_factory[agg_name]

    # Connect to ES and perform query.
    client = get_elasticsearch_client(timeout=settings.ES_SEARCH_TIMEOUT)

    search_body: Dict[Any, Any] = search_dsl.to_dict()
    search_kwargs: Dict[Any, Any] = dict(
        index=index_name, body=search_body, ignore_unavailable=True, human=False
    )
    print(search_body)

    if settings.TEST:
        search_kwargs["body"]["profile"] = True
        search_kwargs["human"] = True
    search_results = client.search(**search_kwargs)
    search_results["query"] = settings.TEST and search_kwargs["body"]
    return search_results


def init_elasticsearch_index(
    client: elasticsearch.client.Elasticsearch, index_name: str
) -> None:
    default_mapping = {
        "item": {
            "properties": {
                "title": {"type": "text"},
                "description": {"type": "text"},
                "category": {"type": "keyword"},
                "sub_category": {"type": "keyword"},
                # Cost mappings
                "cost": {"type": "float"},
                "cost_suggestions": {
                    "properties": {
                        "price": {"type": "float"},
                        "cook_id": {"type": "integer"},
                        "cook_name": {"type": "text"},
                    }
                },
                "orders": {
                    "properties": {
                        "user.id": {"type": "keyword"},
                        "cost": {"type": "float"},
                        "create_date": {"type": "date"},
                        "comment": {"type": "text"},
                        "status": {"type": "text"},
                        "done": {"type": "boolean"},
                        "quantity": {"type": "int"},
                    }
                },
                "is_cook_offer": {"type": "boolean"},
                "is_consumer_offer": {"type": "boolean"},
                "status": {"type": "text"},
                "done": {"type": "boolean"},
                "is_deleted": {"type": "boolean"},
                # Used for resource availability calculation
                "user.id": {"type": "keyword"},
                "user.name": {"type": "keyword"},
                "region": {"type": "text"},
                "create_date": {"type": "date"},
                "comment": {"type": "text"},
            }
        }
    }

    index_settings = {
        "index": {
            "max_inner_result_window": 10000,  # To allow pagination
            "number_of_shards": settings.ELASTICSEARCH_NUMBER_OF_SHARDS,
            "number_of_replicas": settings.ELASTICSEARCH_NUMBER_OF_REPLICAS,
            "refresh_interval": settings.ELASTICSEARCH_REFRESH_INTERVAL,
        },
        "analysis": {
            "analyzer": {
                "default": {"tokenizer": "whitespace", "filter": ["lowercase"]}
            }
        },
    }

    index_body = {"mappings": default_mapping, "settings": index_settings}

    if not isinstance(index_name, str):
        raise ValueError("index_name must be a string")

    try:
        client.indices.create(index=index_name, body=index_body)
        client.indices.refresh(index=index_name)
    except RequestError as e:
        LOG.debug(e)


def update_elasticsearch_items(
    client: elasticsearch.client.Elasticsearch,
    item_list: List[Dict[str, Any]],
    index_name: str = settings.DEFAULT_INDEX,
    refresh: bool = False,
    chunk_size: int = settings.ELASTICSEARCH_INGESTION_CHUNK_SIZE,
) -> bool:
    """
    Update resources in bulk and update last_updated
    timestamp.

    Args:
        client (elasticsearch.client.Elaticsearch): ElasticSearch API client.
        item_list (list): Array of items to ingest/upsert into
            Elasticsearch. Every item must have `doc` or `_source` field and `_id` field.
        index_name (str): Optional parameter and can be omitted in
            favor of `_index` field in every item.

    Returns:
        bool: Status of update
    """
    for item in item_list:

        if not isinstance(index_name, str) and "_index" not in item:
            raise WrongItemFormat(
                "Missing _index key in item or index_name is not a string"
            )

        if "doc" not in item and "_source" in item:
            item["doc"] = item.pop("_source")

        if "doc" not in item:
            raise WrongItemFormat("No _source or doc key found in item")

        if "_id" not in item:
            raise WrongItemFormat("No _id field supplied")

        if index_name and not isinstance(index_name, (list, set)):
            item["_index"] = index_name

        # This part allows partial updates.
        item["_type"] = "item"
        item["_op_type"] = "update"
        item["doc_as_upsert"] = True
        item["doc"]["last_updated"] = datetime.now()

    if not client.indices.exists(index=index_name):
        init_elasticsearch_index(client, index_name)

    try:
        with task_lock(
            key=f"index_update_{index_name}", timeout=300, blocking=True
        ):  # 5 minutes
            response = helpers.bulk(
                client, item_list, chunk_size=chunk_size, max_retries=3,
            )

    except BulkIndexError as e:
        LOG.critical(f"Failed ingestion: {e}")
        response = False

    except Exception as e:
        LOG.critical(f"Unhandled elasticsearch exception: {e}")
        response = False

    if refresh:
        client.indices.refresh(index=index_name)

    return response


def place_order(request, item_id, index_name=settings.DEFAULT_INDEX):
    item = search_fuzzy(filters={"_id": item_id}, index_name=index_name)["hits"]["hits"]

    if not item:
        raise Http404

    item = item[0]
    orders = item["_source"].get("orders", [])
    quantity = int(request.POST.get("quantity", 1))

    order_found = False
    for order in orders:
        print(order)
        if order["user.id"] == request.user.id:
            order["quantity"] += quantity
            order_found = True

    if not order_found:
        orders.append(
            {
                "user.id": request.user.id,
                "user.name": request.user.username,
                "create_date": datetime.now(),
                "comment": request.POST.get("comment"),
                "status": "Open",
                "done": False,
                "quantity": quantity,
            }
        )

    ingestion_document = {
        "_id": item_id,
        "_source": {"orders": orders,},
    }
    client = get_elasticsearch_client()
    update_elasticsearch_items(
        client=client, item_list=[ingestion_document], refresh=True
    )
